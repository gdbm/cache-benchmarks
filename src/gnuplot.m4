divert(-1)
changequote([,])
define([_PLOT],
["$2.log" \
        using 1:8:9:10 \
        title "GDBM $2" with errorbars ls $1, \
     "" using 1:8 \
        notitle \
        smooth bezier ls $1[]dnl
ifelse([$3],,,[dnl
,\
     $0(eval(($1 % 3) + 1), shift(shift($*)))])])
define([PLOT],[plot _PLOT(1, $*)])
divert(0)dnl
set title "Execution time as function of cache size.\n\
Database size NRECS entries. Key set size NKEYS.\n\
Each measurement averages NUMSAMPLES samples."
set xlabel 'Cache entries' 
set ylabel 'Runtime (seconds)'
set style line 1 linecolor rgb "dark-red" linewidth 1.000 dashtype solid pointtype 1 pointsize default pointinterval 0
set style line 2 linecolor rgb "#e69f00" linewidth 1.000 dashtype solid pointtype 1 pointsize default pointinterval 0
set style line 3 linecolor rgb "blue" linewidth 1.000 dashtype solid pointtype 1 pointsize default pointinterval 0
set style line 4 linecolor rgb "#0072b2" linewidth 1.000 dashtype solid pointtype 1 pointsize default pointinterval 0

PLOT(TARGETS)
