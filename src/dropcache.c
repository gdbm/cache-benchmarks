#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

char const drop_caches[] = "/proc/sys/vm/drop_caches";

int
main(int argc, char **argv)
{
  int fd;
  fd = open(drop_caches, O_WRONLY);
  if (fd == -1)
    {
      perror(drop_caches);
      return 1;
    }
  sync();
  if (write(fd, "3\n", 2) != 2)
    {
      perror(drop_caches);
      return 1;
    }      
  close(fd);
  return 0;
}
